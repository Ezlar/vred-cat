# Generated by Django 3.0.6 on 2020-06-02 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GfycatUrl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vred_url', models.CharField(max_length=255)),
                ('gfy_url', models.CharField(max_length=255)),
            ],
        ),
    ]
