from django import forms

class UrlForm(forms.Form):
  reddit_url = forms.URLField(label = '', widget= forms.TextInput(attrs={'class':'u-full-width'}))