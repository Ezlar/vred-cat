from celery import Celery,shared_task
from celery_progress.backend import ProgressRecorder
try:
    from .stripper import Processor
except ImportError:
    from stripper import Processor

app = Celery('tasks',
             backend='redis://localhost:6830',
             broker='redis://localhost:6830//')


@shared_task(bind=True,name='vidconv.tasks.processing')
def Processing(self,address):
    progress_recorder = ProgressRecorder(self)
    converter = Processor(address)
    converter.strip_fallback()
    record = converter.database_find()
    progress_recorder.set_progress(1, 100, description='Searching exisiting records')
    if record == None:
        progress_recorder.set_progress(10, 100, description='Downloading video')
        converter.mux()
        if not converter.audio_url: 
          progress_recorder.set_progress(20, 100, description='Uploading to Gfycat')
          retval = converter.gfycat_upload(progress_recorder)
        else:
          progress_recorder.set_progress(20, 100, description='Uploading to Streamable')
          converter.streamable_upload()
          retval = 30
        progress_recorder.set_progress(retval, 100, description='Updating Database')
        converter.database_insert()
        progress_recorder.set_progress(retval+10, 100, description='Retrieving URL')
        record = converter.database_find()
        converter.clean_dir()
        
    retval = record['gfycat'] if not converter.audio_url else record['streamable']
    return retval
