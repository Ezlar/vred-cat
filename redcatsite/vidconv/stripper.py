import requests
import io
import sys
import json
import re
import wget
import subprocess

from pymongo import MongoClient
from monsterurl import get_monster
from celery_progress.backend import ProgressRecorder
from pystreamable import StreamableApi

'''
JsonStripper takes a reddit video link as input.
* Serches the json file using regex for the links to the audio and video ✓
* Downloads the two separately using wget ✓
* Combines into one mp4 file using ffmpeg ✓
* Uploads the file to gfycat ✓
* Adds a record to the database for the provided reddit link and gfycat url ✓
'''

# TODO Add a Streamable upload function to hosts videos that have audio instead of in gyfcat. 
class Processor:

    # Prepare client for http requests and database connections
    def __init__(self, url):
        # This is hardcoded for specific kinds of reddit urls #TODO allow different kind of reddit links
        self.json_url = url + "/.json"
        self.url = url
        self.user_agent = {'User-Agent': 'vredt-cat'}
        self.client = MongoClient()
        self.db = self.client['vred-cat']

    def strip_fallback(self):
        data = requests.get(url=self.json_url, headers=self.user_agent).json()
        # TODO insert assertion here to ensure it is a reddit video post
        self.fallback_url = data[0]['data']['children'][0]['data']['secure_media']['reddit_video']['fallback_url']
        
        if self.fallback_url.find('.mp4') != -1:
          url_split = self.fallback_url.split('_')
          self.audio_url = url_split[0] + '_audio.mp4'
        else:
          url_split = self.fallback_url.split('DASH')
          self.audio_url = url_split[0] + 'audio'

        code = requests.get(self.audio_url).status_code
        print(self.audio_url)
        print(code)
        # TODO 200 should be the only code allowed, will test further
        acceptable_codes = [200]  # ,405
        if code not in acceptable_codes:
            self.audio_url = None

    # Download the audio and video and combine them

    def mux(self):
        self.outputfile = './{}.mp4'.format(get_monster())
        if self.audio_url == None:
            wget.download(self.fallback_url, self.outputfile, False)
        else:
            audiofile = './{}.mp3'.format(get_monster())
            videofile = './{}.mp4'.format(get_monster())
            wget.download(self.fallback_url, videofile, False)
            wget.download(self.audio_url, audiofile, False)
            subprocess.call(['ffmpeg', '-loglevel', 'error',
                             '-i', videofile, '-i', audiofile, self.outputfile])

    # Authenticate with gfycat and then upload the combined mp4 file
    def gfycat_upload(self, recorder):
        body = {
            "grant_type": "client_credentials",
            "client_id": '2_Fl5wGo',
            "client_secret": 'mvM0RRieWqSprGUSBPpSFy5HIcAuETC93fUhPRz0pbb5YccxCGw3PQV_m7et_spX',
        }

        token = requests.post(
            "https://api.gfycat.com/v1/oauth/token", json=body, timeout=3).json()
        access_token = token["access_token"]
        auth_headers = {
            "Authorization": "Bearer {}".format(access_token)}

        params = {}
        params['keepAudio'] = True

        create = requests.post(
            "https://api.gfycat.com/v1/gfycats", headers=auth_headers, data=str(params))
        gfyid = create.json().get("gfyname")

        if gfyid:
            self.link = "https://gfycat.com/{}".format(gfyid)

            with open(self.outputfile, "rb") as video:
                resp = requests.put(
                    "https://filedrop.gfycat.com/{}".format(gfyid), video)
                if resp.status_code != 200:
                    print("Error uploading to Gfycat!")

            status = requests.get(
                "https://api.gfycat.com/v1/gfycats/fetch/status/{}".format(gfyid)).json()
            i = 0
            # TODO: Also check for other kinds of returns to avoid infinte loops
            # TODO: For the love of god do not leave this unclean.
            while status['task'] != 'complete':
                status = requests.get(
                    "https://api.gfycat.com/v1/gfycats/fetch/status/{}".format(gfyid)).json()
                print(i)
                i += 1
                recorder.set_progress(
                    20+i, 100, description="Uploading to Gyfcat")
            return 20+i

    def streamable_upload(self):
      api = StreamableApi('xessgsqsvtfbidvnow@twzhhq.online','lqTec87sLu9xiEyGpAde')
      req = api.upload_video(self.outputfile)
      if req['status'] == 1:
        self.link = "https://streamable.com/{}".format(req['shortcode'])

  
    # Checks if the url has already been processed
    def database_find(self):
        return self.db.gifs.find_one({'url': self.fallback_url})

    def clean_dir(self):
        subprocess.call(['make', '-s', 'clean'])

    # Log the url and it's respective gfycat link
    def database_insert(self):
        platform = 'gfycat' if not self.audio_url  else 'streamable'    
        entry = {
            'url': self.fallback_url,
            platform: self.link
        }
        self.db.gifs.insert_one(entry)
