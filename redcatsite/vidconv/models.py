from django.db import models

# Create your models here.


class GfycatUrl(models.Model):
    vred_url = models.CharField(max_length=255)
    gfy_url = models.CharField(max_length=255)

    def __str__(self):
        return self.gfy_url
