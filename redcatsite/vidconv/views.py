from django.shortcuts import render
from django.http import HttpResponse
from django.core.validators import URLValidator
from django.core.validators import ValidationError

from .tasks import Processing

# Create your views here.


def home(request):
    if request.method == 'POST':
        if is_valid_url(request.POST.get('url')):
            address = request.POST.get('url')
            res = Processing.delay(address)
            task_id = res.task_id
            print(task_id)
            return render(request, 'vidconv/progress.html', {'task_id':task_id})
    return render(request, 'vidconv/index.html')


def is_valid_url(url):

    validate = URLValidator()
    try:
        validate(url)
        return True
    except ValidationError:
        return False
